<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', function () {
        return view('/home');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('asesors', 'AsesorController@index');
Route::get('asesors/{id}', 'AsesorController@show');

Route::resource('empresas','EmpresaController');
Route::resource('proyectos','ProyectoController');

Route::get('alumnos', 'AlumnoController@index');
Route::get('alumnos/{id}', 'AlumnoController@show');

Route::get('servicio/{id}', 'ServicioController@create');
Route::post('servicio', 'ServicioController@store');
Route::get('servicio/detalle/{id}', 'ServicioController@show');

Route::get('servicio/edit/{id}', 'ServicioController@edit');
Route::post('servicio/edit/{id}', 'ServicioController@update');



Route::get('profile', 'UserController@profile');
Route::post('profile', 'UserController@update_avatar');


Route::get('select2-proyectos', 'Select2Controller@proyectos');

Route::get('select2-asesores', 'Select2Controller@asesores');
}
);