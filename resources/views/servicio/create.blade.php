@extends('layouts.app')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <h3>Inicio de proceso servicio social</h3>

      <div class="card">
        <h6 class="card-header">{{$alumno['no_exp']}} | {{$alumno['nombre']}} {{$alumno['apell_pat']}}
          {{$alumno['apell_mat']}} </h6>

        <div class="card-body">
          <form method="post" action="{{url('servicio')}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
            <div class="col-md-1"></div>
              <div class="col-md-10">
                <div class="form-group">
                    <input class="form-control" value="{{$alumno['no_exp']}}" type="hidden" name="expediente" maxlength="30" required>
                  <label for="inputdate">Proyecto <i class="fas fa-project-diagram"></i></label>
                  <select class="proyecto form-control" name="proyecto_id"></select>
                </div>
              </div>
              <div class="col-md-1"></div>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
                <div class="col-md-10">
                  <div class="form-group">
                    <label for="inputdate">Asesor/Tutor | Nombre | ID (Opcional) <i class="fas fa-user"></i></label>
                   
                    
                  <select class="asesor form-control" name="asesor_id"></select>
                  </div>
                </div>
                <div class="col-md-1"></div>
              </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="inputdate">Fecha de inicio <i class="fa fa-calendar" aria-hidden="true"></i></label>

                  <input class="form-control" autocomplete="off" type="date" min="2000-01-01" placeholder="YYYY/MM/DD" name="dia_inicio"
                    maxlength="30" required>
                </div>
              </div>
              
              <div class="col-md-3">
                <div class="form-group">
                  <label for="input-group">Hora de inicio <i class="fas fa-clock"></i></label>
                  <input class="form-control" autocomplete="off" type="time" placeholder="" name="hora_inicio" required></div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="input-group">Hora salida <i class="fas fa-clock"></i></label>

                  <input class="form-control" autocomplete="off" type="time" placeholder="" name="hora_final" required></div>
              </div>
            </div>
        </div>

        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Enviar</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">


  $('.proyecto').select2({
    placeholder: 'Seleccione un proyecto',
    allowClear: true,
    ajax: {
      url: '/select2-proyectos',
      dataType: 'json',
  //    delay: 100,
      processResults: function (data) {

        return {
          results:  $.map(data, function (item) {
                return {
                    text: item.id+".- "+item.nombre,
                    id: item.id
                }
            })
        };
      },
      cache: true
    }
  });

  $('.asesor').select2({
    placeholder: 'Seleccione un asesor',
    allowClear: true,
    ajax: {
      url: '/select2-asesores',
      dataType: 'json',
     // delay: 100,
      processResults: function (data) {

        data = JSON.parse(JSON.stringify(data).replace(/\:null/gi, "\:\"\"")); 
        return {
          results:  $.map(data, function (item) {
                return {
                    text: item.num_emp+".- "+item.nombre+" "+item.ap_pat+" "+item.ap_mat,
                    id: item.num_emp
                }
            })
        };
      },
      cache: true
    }
  });


</script>
@endsection