<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Registro mensual de horas</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="post" action="{{action('ListaHorasController@store', $id)}}">

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                          <div class="form-group">
                                    <label for="input-group">Horas <i class="fas fa-clock"></i></label>
                                    <input class="form-control" autocomplete="off" type="number" placeholder="" name="horas" required></div>
                    </div>

                    <div class="col-md-4">
                            <div class="form-group">
                              <label for="inputdate">Fecha de inicio <i class="fa fa-calendar" aria-hidden="true"></i></label>
            
                              <input class="form-control"  autocomplete="off" type="date" min="2000-01-01" placeholder="YYYY/MM/DD" name="dia_inicio"
                                maxlength="30" required>
                            </div>
                          </div>
                          <div class="col-md-4">
                                <div class="form-group">
                                  <label for="inputdate">Fecha final <i class="fa fa-calendar" aria-hidden="true"></i></label>
                
                                  <input class="form-control" autocomplete="off" type="date" min="2000-01-01" placeholder="YYYY/MM/DD" name="dia_inicio"
                                    maxlength="30" required>
                                </div>
                              </div>
                </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
              <button type="submit" class="btn btn-success">Registrar</button>
            </div>
            </form>
          </div>
        </div>
      </div>