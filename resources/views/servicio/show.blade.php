@extends('layouts.app')
@section('content')
<div class="container">  
<h3>Proceso Servicio Social | S-{{$id}}</h3>
  <div class="row justify-content-center">
     
    <div class="col-md-8">
       

      <div class="card">
        <h6 class="card-header">{{$alumno['no_exp']}} | {{$alumno['nombre']}} {{$alumno['apell_pat']}}
          {{$alumno['apell_mat']}} </h6>

        <div class="card-body">
          <form method="post"  enctype="multipart/form-data">
            @csrf
            <div class="row">
            <div class="col-md-1"></div>
              <div class="col-md-10">
                <div class="form-group">
                    <input class="form-control" value="{{$alumno['no_exp']}}" type="hidden" name="expediente" maxlength="30" required>
                  <label for="inputdate">Proyecto <i class="fas fa-project-diagram"></i></label>
                  <select class="proyecto form-control" name="proyecto_id"></select>
                </div>
              </div>
              <div class="col-md-1"></div>
            </div>
            <div class="row">
              <div class="col-md-1"></div>
                <div class="col-md-10">
                  <div class="form-group">
                    <label for="inputdate">Asesor/Tutor | Nombre | ID (Opcional) <i class="fas fa-user"></i></label>
                   
                    
                  <select class="asesor form-control" name="asesor_id"></select>
                  </div>
                </div>
                <div class="col-md-1"></div>
              </div>
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="inputdate">Fecha de inicio <i class="fa fa-calendar" aria-hidden="true"></i></label>

                  <input class="form-control" value={{$servicio->dia_inicio}} autocomplete="off" type="date" min="2000-01-01" placeholder="YYYY/MM/DD" name="dia_inicio"
                    maxlength="30" disabled required>
                </div>
              </div>
              
              <div class="col-md-3">
                <div class="form-group">
                  <label for="input-group">Hora de inicio <i class="fas fa-clock"></i></label>
                  <input class="form-control" value={{$servicio->hora_inicio}} autocomplete="off" type="time" placeholder="" name="hora_inicio" disabled ></div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label for="input-group">Hora salida <i class="fas fa-clock"></i></label>

                  <input class="form-control" value={{$servicio->hora_final}} autocomplete="off" type="time" placeholder="" name="hora_final" disabled ></div>
              </div>
            </div>
        </div>

       
        </form>

      </div>
    </div>
    <div class="col-md-4">
        
        <div class="card">
            <div class="card-header">
              Reporte de horas mensuales
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">80</li>
              <li class="list-group-item">80</li>
              <li class="list-group-item">80</li>
            </ul>
          </div>


    </div>



  </div>
  <br>
  <div class="row justify-content-md-center">
      <td><a href="{{action('AlumnoController@show', $alumno['no_exp'])}}"  class="btn btn-danger" title="Regresar" >Regresar</a></td>
  </div>
</div>
</div>




<script type="text/javascript">


  $('.proyecto').select2({
    placeholder: 'Seleccione un proyecto',
    allowClear: true,
    width: '100%',
    disabled: true,
    ajax: {
      url: '/select2-proyectos',
      dataType: 'json',
     
  //    delay: 100,
      processResults: function (data) {

        return {
          results:  $.map(data, function (item) {
                return {
                    text: item.id+".- "+item.nombre,
                    id: item.id
                }
            })
        };
      },
      cache: true
    }
  });

  var select = $('.proyecto');
  var nombre=<?php echo "'".$proyecto_nombre."'"; ?>;
  var option = new Option(nombre, {{$servicio->proyecto}}, true, true);
  select.append(option).trigger('change');


  var select = $('.asesor');
  var nombre=<?php echo "'".$asesor_nombre."'"; ?>;
  var option = new Option(nombre, {{$servicio->asesor}}, true, true);
  select.append(option).trigger('change');





  $('.asesor').select2({
    placeholder: 'Seleccione un asesor',
    allowClear: true,
    disabled: true,
    width: '100%',
    ajax: {
      url: '/select2-asesores',
      dataType: 'json',
     
     // delay: 100,
      processResults: function (data) {

        data = JSON.parse(JSON.stringify(data).replace(/\:null/gi, "\:\"\"")); 
        return {
          results:  $.map(data, function (item) {
                return {
                    text: item.num_emp+".- "+item.nombre+" "+item.ap_pat+" "+item.ap_mat,
                    id: item.num_emp
                }
            })
        };
      },
      cache: true
    }
  });

  

</script>
@endsection