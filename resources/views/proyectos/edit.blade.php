@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header">Editar empresa</h5>

                <div class="card-body">
                  <form method="post" action="{{action('ProyectoController@update', $id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group col-md-8">
                            <label for="Nombre">Nombre:</label>
                            <input type="text" class="form-control" value="{{$proyecto->nombre}}"  name="nombre">
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group col-md-8">
                            <label for="Titular">Titular:</label>
                            <input type="text" class="form-control" value="{{$proyecto->titular}}"  name="titular">
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group col-md-8">
                            <label for="Dirección">Dirección:</label>
                            <input type="text" class="form-control" value="{{$proyecto->direccion}}"  name="direccion">
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group col-md-8">
                            <label for="Asesor">Asesor:</label>
                            <input type="text" class="form-control" value="{{$proyecto->asesor}}" name="asesor">
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group col-md-8">
                            <label for="Receptora">Receptora:</label>
                            <input type="text" class="form-control" value="{{$proyecto->receptora}}"  name="receptora">
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="form-group col-md-8">
                            <label for="Cargo">Cargo:</label>
                            <input type="text" class="form-control" value="{{$proyecto->cargotitular}}"  name="cargotitular">
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group col-md-8">
                          <label for="Tipo">Tipo:</label>
                          <input type="number" class="form-control" value="{{$proyecto->tipo}}"  name="tipo">
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group col-md-8">
                          <label for="Carreras">Carreras:</label>
                          <input type="text" class="form-control" value="{{$proyecto->carreras}}"  name="carreras">
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group col-md-8">
                          <label for="Integrantes">Integrantes:</label>
                          <input type="number" class="form-control" value="{{$proyecto->integrantes}}"  name="integrantes">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2"></div>
                        <div class="form-group col-md-8">
                          <label for="Actividades">Actividades:</label>
                          <input type="text" class="form-control" value="{{$proyecto->actividades}}"  name="actividades">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-2"></div>
                          <div class="form-group col-md-8">
                            <label for="Telefono">Telefono:</label>
                            <input type="text" class="form-control" value="{{$proyecto->telefono}}"  name="telefono">
                          </div>
                      </div>
                        <div class="row">
                          <div class="col-md-4"></div>
                          <div class="form-group col-md-4" style="margin-top:60px">
                            <button type="submit" class="btn btn-success">Enviar</button>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
