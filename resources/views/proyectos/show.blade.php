@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header">Detalles proyecto</h5>

                <div class="card-body">
                   
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Nombre:</label><br>
                      <label for="nombre">{{$proyecto->nombre}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Titular:</label><br>
                      <label for="telefono">{{$proyecto->titular}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Dirección:</label><br>
                      <label for="direccion">{{$proyecto->direccion}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Asesor:</label><br>
                      <label for="ciudad">{{$proyecto->asesor}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Receptora:</label><br>
                      <label for="responsable">{{$proyecto->receptora}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Cargo:</label><br>
                      <label for="puesto">{{$proyecto->cargotitular}}</label>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Tipo:</label><br>
                      <label for="puesto">{{$proyecto->tipo}}</label>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group col-md-8">
                    <label for="Name">Carreras:</label><br>
                    <label for="puesto">{{$proyecto->carreras}}</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group col-md-8">
                    <label for="Name">Integrantes:</label><br>
                    <label for="puesto">{{$proyecto->integrantes}}</label>
                  </div>
                </div>       
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group col-md-8">
                    <label for="Name">Actividades:</label><br>
                    <label for="puesto">{{$proyecto->actividades}}</label>
                  </div>
                </div>       
                <div class="row">
                  <div class="col-md-2"></div>
                  <div class="form-group col-md-8">
                    <label for="Name">Telefono:</label><br>
                    <label for="puesto">{{$proyecto->telefono}}</label>
                  </div>
                </div>       
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
