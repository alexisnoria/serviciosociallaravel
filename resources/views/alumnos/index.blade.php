@extends('layouts.app')

@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <h5 class="card-header">Alumnos</h5>

                <div class="card-body">

                <form method="GET" action="/alumnos">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" id="search" name="search" class="form-control" placeholder="Ingresa nombre" value="{{ old('search') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <button class="btn btn-success"><i class="fas fa-search"></i> Buscar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>

                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Expediente</th>
                            <th>Nombre</th>
                            <th>Telefono</th>
                            <th>Correo</th>
                            <th colspan="2">Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if($alumnos->count())
                            @foreach($alumnos as $alumno)
                            <tr>
                              <td>{{$alumno['no_exp']}}</td>
                              <td>{{$alumno['nombre']}} {{$alumno['apell_pat']}} {{$alumno['apell_mat']}}</td>
                              <td>{{$alumno['telefono']}}</td>
                              <td>{{$alumno['email']}}</td>
                            
                              <td><a href="{{action('AlumnoController@show', $alumno['no_exp'])}}"  class="btn btn-primary" title="Detalles" ><i class="fas fa-info"></i></a></td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">No se encontraron datos.</td>
                            </tr>
                            @endif
                        </tbody>
                      </table>
                    </div>

                    @if($alumnos instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        {{ $alumnos->links() }} 
                      
                     @endif
                     Total: {{ $alumnos->count() }}
                </div>
            </div>
        </div>
    </div>
</div>
<script>
      $(function() {
            $("#search").focus();
        });
  </script>
@endsection
