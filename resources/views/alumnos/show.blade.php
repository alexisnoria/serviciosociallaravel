@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">{{$alumno['no_exp']}} </div>
                <div class="card-body">
                    <h5 class="card-title">{{$alumno['nombre']}} {{$alumno['apell_pat']}} {{$alumno['apell_mat']}}</h5>
                    Email
                    <p class="card-text strong">{{$alumno['email']}}</p>
                    Teléfono
                    <p class="card-text">{{$alumno['telefono']}}</p>
                    Celular
                    <p class="card-text">{{$alumno['celular']}}</p>
                    Fecha de nacimiento
                    <p>{{ \Carbon\Carbon::parse($alumno['fech_nac'])->isoFormat('D MMMM YYYY')}}</p>
                </div>

            </div>
        </div>


        <div class="col-md-9">
            <div class="card">
                <ul class="nav nav-pills  mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#nav-home" role="tab"
                            aria-controls="pills-home" aria-selected="true">Servicio Social</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#nav-profile" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Practica I</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#nav-contact" role="tab"
                            aria-controls="pills-contact" aria-selected="false">Practica II</a>
                    </li>
                </ul>

                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active container" id="nav-home" role="tabpanel"
                        aria-labelledby="nav-home-tab">

                        @if($servicioSocial)

                            @if($estado!=EN_PROCESO)
                                @if($estado!=TERMINADO)
                                <a href="{{action('ServicioController@create', $alumno['no_exp'])}}" class="btn btn-success"
                                style="float: left;"><i class="fas fa-play"></i> Iniciar</a>
                                @endif
                            @endif 

                            @if(!$registrosServicio->isEmpty())
                            <div class="table-responsive-md">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th >Folio</th>
                                        <th >Proyecto</th>
                                        <th >Asesor</th>
                                        <th >Inicio</th>
                                        <th >Estado</th>
                                        <th >Detalle</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($registrosServicio as $registro)
                                    <tr>
                                        <td>S-{{ $registro->id_servicio }}</td>
                                        <td >{{ $proyecto[$registro->id_servicio][1] }}</td>
                                        <td >{{ $asesor[$registro->id_servicio][1] }} {{ $asesor[$registro->id_servicio][2] }}</td>
                                      
                                      
                                    <td> {{ \Carbon\Carbon::parse($registro->dia_inicio)->isoFormat('D MMMM YYYY')}}</td>
                                        @switch($registro->estado)
                                            @case(1)
                                                <td>En proceso</td>
                                            @break
                                            @case(2)
                                                <td>Terminado</td>
                                            @break
                                            @case(3)
                                                <td>Cancelado</td>
                                            @break
                                        @endswitch
                                        <td ><a href="{{action('ServicioController@show', $registro->id_servicio)}}"  class="btn btn-primary" title="Detalles" ><i class="fas fa-info-circle"></i></a>
                                          @if($registro->estado==1)
                                          <a href="{{action('ServicioController@edit', $registro->id_servicio)}}"  class="btn btn-warning" title="Editar" ><i class="fas fa-edit"></i></a>
                                          @endif
                                        </td>

                                          
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                            @endif
                        @else

                        <div class="alert alert-danger" role="alert">
                            No disponible
                        </div>

                        @endif

                    </div>
                    <div class="tab-pane fade container" id="nav-profile" role="tabpanel"
                        aria-labelledby="nav-profile-tab">
                      
                    </div>
                    <div class="tab-pane fade container" id="nav-contact" role="tabpanel"
                        aria-labelledby="nav-contact-tab">
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
@endsection