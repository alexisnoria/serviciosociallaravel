@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <h5 class="card-header">Asesores </h5>

                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            
                            <th colspan="2">Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if($asesors->count())
                            @foreach($asesors as $asesor)
                            <tr>
                              <td>{{$asesor['num_emp']}}</td>
                              <td>{{$asesor['nombre']}} {{$asesor['ap_pat']}} {{$asesor['ap_mat']}}</td>
                            
                              <td><a href="{{action('AsesorController@show', $asesor['num_emp'])}}" class="btn btn-primary"><i class="fas fa-info-circle"></i> Detalles</a></td>
                              
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">No se encontraron datos.</td>
                            </tr>
                            @endif
                        </tbody>
                      </table>
                    </div>


                    {{ $asesors->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
