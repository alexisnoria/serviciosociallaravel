@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header">Editar asesor</h5>

                <div class="card-body">
                    <form method="post" action="{{action('AsesorController@update', $id)}}">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">
                        <div class="row">
                          <div class="col-md-4"></div>
                          <div class="form-group col-md-4">
                            <label for="Name">Nombre:</label>
                            <input type="text" class="form-control" name="nombre" value="{{$asesor->nombre}}">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4"></div>
                            <div class="form-group col-md-4">
                              <label for="Email">Programa educativo:</label>
                              <input type="text" class="form-control" name="pe" value="{{$asesor->pe}}">
                            </div>
                          </div>
                        
                        <div class="row">
                          <div class="col-md-4"></div>
                          <div class="form-group col-md-4" style="margin-top:60px">
                            <button type="submit" class="btn btn-success">Enviar</button>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
