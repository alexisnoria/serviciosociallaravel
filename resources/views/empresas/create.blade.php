@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header">Crear empresa</h5>

                <div class="card-body">
                    <form method="post" action="{{url('empresas')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-8">
                              <label for="Name">Nombre:</label>
                              <input type="text" class="form-control" name="nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-8">
                              <label for="Name">Teléfono:</label>
                              <input type="text" class="form-control" name="telefono">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-8">
                              <label for="Name">Dirección:</label>
                              <input type="text" class="form-control" name="direccion">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-8">
                              <label for="Name">Ciudad:</label>
                              <input type="text" class="form-control" name="ciudad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-8">
                              <label for="Name">Responsable:</label>
                              <input type="text" class="form-control" name="responsable">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="form-group col-md-8">
                              <label for="Name">Puesto/Cargo:</label>
                              <input type="text" class="form-control" name="puesto">
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4"></div>
                          <div class="form-group col-md-4" style="margin-top:60px">
                            <button type="submit" class="btn btn-success">Enviar</button>
                          </div>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
