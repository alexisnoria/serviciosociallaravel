@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header">Detalles asesor</h5>

                <div class="card-body">
                   
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Nombre:</label><br>
                      <label for="nombre">{{$empresa->nombre}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Teléfono:</label><br>
                      <label for="telefono">{{$empresa->telefono}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Dirección:</label><br>
                      <label for="direccion">{{$empresa->domicilio}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Ciudad:</label><br>
                      <label for="ciudad">{{$empresa->ciudad}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Responsable:</label><br>
                      <label for="responsable">{{$empresa->responsables}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="form-group col-md-8">
                      <label for="Name">Puesto/Cargo:</label><br>
                      <label for="puesto">{{$empresa->puesto}}</label>
                    </div>
                </div>
                        
                       
                      
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
