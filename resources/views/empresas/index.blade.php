@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <h5 class="card-header">Empresas <a href="{{action('EmpresaController@create')}}" class="btn btn-success" style="float: right;"><i class="fas fa-plus"></i> Crear</a></h5>

                <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Telefono</th>
                            <th>Domicilio</th>
                            <th colspan="2">Acciones</th>
                          </tr>
                        </thead>
                        <tbody>
                          @if($empresas->count())
                            @foreach($empresas as $empresa)
                            <tr>
                              <td>{{$empresa['id']}}</td>
                              <td>{{$empresa['nombre']}}</td>
                              <td>{{$empresa['telefono']}}</td>
                              <td>{{$empresa['domicilio']}}</td>
                            
                              <td><a href="{{action('EmpresaController@show', $empresa['id'])}}" class="btn btn-primary"><i class="fas fa-info-circle"></i> Detalles</a></td>
                              <td><a href="{{action('EmpresaController@edit', $empresa['id'])}}" class="btn btn-warning"><i class="far fa-edit"></i> Editar</a></td>
                              <td>
                                <form action="{{action('EmpresaController@destroy', $empresa['id'])}}" method="post">
                                  @csrf
                                  <input name="_method" type="hidden" value="DELETE">
                                  <button class="btn btn-danger" onclick="return confirm('¿Está seguro?')" type="submit"><i class="far fa-trash-alt"></i> Eliminar</button>
                                </form>
                              </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="4">No se encontraron datos.</td>
                            </tr>
                            @endif
                        </tbody>
                      </table>
                    </div>
                    {{ $empresas->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
