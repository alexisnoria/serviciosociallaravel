<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicioHorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio_horas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_servicio');
            $table->date('dia_inicio'); //DIA INICIO SERVICIO SOCIAL
            $table->date('dia_final'); //DIA FINAL SERVICIO SOCIAL
            $table->integer('horas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio_horas');
    }
}
