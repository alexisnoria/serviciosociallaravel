<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicio', function (Blueprint $table) {
            $table->increments('id_servicio');
            $table->string('expediente');//EXPEDIENTE DE ALUMNO
            $table->integer('proyecto'); //PROYECTO DEL SERVICIO SOCIAL 
            $table->integer('asesor'); //PROYECTO DEL SERVICIO SOCIAL 
            $table->date('dia_inicio'); //DIA INICIO SERVICIO SOCIAL
            $table->date('dia_final')->nullable(); //DIA FINAL SERVICIO SOCIAL
            $table->time('hora_inicio');//HORA INICIO SERVICIO SOCIAL
            $table->time('hora_final');//HORA FINAL SERVICIO SOCIAL
            $table->integer('horas_totales')->nullable(); //HORAS COMPLETADAS POR ALUMNO
            $table->integer('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicio');
    }
}
