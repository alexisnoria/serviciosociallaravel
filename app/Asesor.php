<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asesor extends Model
{
     /**
     * The database connection used by the model.
     *
     * @var string
     */  
   // protected $connection = 'ueshermosillo';
   protected $connection = 'ueshermosillo';
   
   protected $table = 'rechum_sl.rh_personal';
     
   protected $primaryKey = 'num_emp';
     
    /**
    * The database table used by the model.
    *
    * @var string
    */
    public function servicio()
    {
        return $this->belongsTo('App\Servicio','asesor','num_emp');
    }
}
