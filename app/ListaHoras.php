<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaHoras extends Model
{
    protected $table = 'servicio_horas';

    public function servicio()
    {
        return $this->belongsTo(Servicio::class, 'id_servicio');
    }

}
