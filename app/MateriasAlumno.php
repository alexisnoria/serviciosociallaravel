<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateriasAlumno extends Model
{
    protected $connection = 'ueshermosillo';
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'ce_alum_calif';

    protected $primaryKey = 'no_exp';

    public function noticia() {
        return $this->belongsTo('ce_alumnos', 'no_exp');
    }
}
