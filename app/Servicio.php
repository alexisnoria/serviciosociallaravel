<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey='id_servicio';
    protected $table = 'servicio';
    //

    public function alumno()

    {
        return $this->belongsTo('App\Alumno', 'expediente','no_exp');
    }

    public function datos_proyecto()
    {
        return $this->hasOne('App\Proyecto','id','proyecto')->withDefault();
    }

    public function datos_asesor()
    {

        return $this->hasOne('App\Asesor','num_emp','asesor');
    }

    public function horas()
    {
        return $this->hasMany(ListaHoras::class);
    } 

}
