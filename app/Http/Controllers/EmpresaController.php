<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas=\App\Empresa::orderBy('id', 'DESC')->paginate(20);

       
        return view('/empresas/index',compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('/empresas/create');
    }
  
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empresa= new \App\Empresa;
        $empresa->nombre=$request->get('nombre');
        $empresa->telefono=$request->get('telefono');
        $empresa->domicilio=$request->get('direccion');
        $empresa->ciudad=$request->get('direccion');
        $empresa->responsables=$request->get('responsable');
        $empresa->puesto=$request->get('puesto');

        $empresa->save();
        
        return redirect('empresas')->with('success', 'Empresa registrada correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = \App\Empresa::find($id);
        return view('empresas/show',compact('empresa','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = \App\Empresa::find($id);
        return view('empresas/edit',compact('empresa','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $empresa= \App\Empresa::find($id);
        $empresa->nombre=$request->get('nombre');
        $empresa->telefono=$request->get('telefono');
        $empresa->domicilio=$request->get('direccion');
        $empresa->ciudad=$request->get('direccion');
        $empresa->responsables=$request->get('responsable');
        $empresa->puesto=$request->get('puesto');

        $empresa->save();
        
        return redirect('empresas')->with('success','Empresa actualizada correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = \App\Empresa::find($id);
        $empresa->delete();
        return redirect('empresas')->with('success','Empresa eliminada correctamente!');
    }
}
