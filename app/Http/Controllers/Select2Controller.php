<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use DB;

class Select2Controller extends Controller

{

    protected $unidad;
    public function __construct(Request $request)
    {
        
        $this->middleware(function ($request, $next) {
            $this->unidad = Auth::user()->unidad;
            // Se elige el nombre de la base segun la unidad del usuario
            switch ($this->unidad) {
                case 1: $nombre="rechum_sl";break;
                case 2: $nombre="rechum_hillo";break;
                case 3: $nombre="rechum_nav";break;
                case 4: $nombre="rechum_mag";break;
                case 5: $nombre="rechum_bj";break;
                case 7: $nombre="rechum_isspe";break;
                default:$nombre="rechum_sl";break;
            }
            DB::disconnect('ueshermosillo');//Se desconecta de la base por defecto
            Config::set('database.connections.ueshermosillo.database', $nombre);//new database name, you want to connect to.
            return $next($request);
        });
    }




    /**

     * Show the application layout.

     *

     * @return \Illuminate\Http\Response

     */

    /**

     * Show the application dataAjax.

     *

     * @return \Illuminate\Http\Response

     */

    public function proyectos(Request $request)

    {

    	$data = [];


        if($request->has('q')){

            $search = $request->q;

            $data = DB::table("proyectos")

            		->select("id","nombre","titular")
                    ->where('nombre','LIKE',"%$search%" )
                    ->Orwhere( 'titular','LIKE',"%$search%") 
                    ->Orwhere( 'id','LIKE',"%$search%") 
            		->get();
        }else {
            $data = DB::table("proyectos")

            ->select("id","nombre","titular")
            ->get();
        }


        return response()->json($data);

    }

  



      public function asesores(Request $request)

    {
        
    	$data = [];

        if($request->has('q')){

            $search = $request->q;

            
            $data = \App\Asesor::where('espadre', 1)->
            where('num_emp', 'LIKE', '%' . $search . '%')->
            where('espadre', 1)->
            orWhere('nombre', 'LIKE', '%' . $search . '%')->
            where('espadre', 1)->
            orWhere('ap_pat', 'LIKE', '%' . $search . '%')->
            where('espadre', 1)->
            orWhere('ap_mat', 'LIKE', '%' . $search . '%')->
            get();



            
        }else {
            $data = \App\Asesor::get();
        }


        return response()->json($data);

    }

}