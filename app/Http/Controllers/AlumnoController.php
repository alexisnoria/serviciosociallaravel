<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class AlumnoController extends Controller
{
    protected $unidad;
    
    public function __construct(Request $request)
    {
       
        $this->middleware(function ($request, $next) {
            $this->unidad = Auth::user()->unidad;
            // Se elige el nombre de la base segun la unidad del usuario
            switch ($this->unidad) {
                case 1: $nombre="escslactual";break;
                case 2: $nombre="eschilloactual";break;
                case 3: $nombre="escnavactual";break;
                case 4: $nombre="escmagactual";break;
                case 5: $nombre="escbjactual";break;
                case 7: $nombre="escisspeactual";break;
                default:$nombre="escslactual";break;
            }
            DB::disconnect('ueshermosillo');//Se desconecta de la base por defecto
            Config::set('database.connections.ueshermosillo.database', $nombre);//new database name, you want to connect to.
            return $next($request);
        });
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search')){
            $busqueda = ltrim($_GET['search']);
            $busqueda= rtrim($busqueda);

            if($busqueda==""){
                $alumnos= \App\Alumno::orderBy('no_exp', 'ASC')->paginate(25);
            }else{

                $alumnos = \App\Alumno::where('no_exp', 'LIKE', '%' . $busqueda . '%')->
                orWhere('nombre', 'LIKE', '%' . $busqueda . '%')->
                orWhere('apell_pat', 'LIKE', '%' . $busqueda . '%')->
                orWhere('apell_mat', 'LIKE', '%' . $busqueda . '%')->
                orWhere('email', 'LIKE', '%' . $busqueda . '%')->
                get();
            }
        }else{
            $alumnos= \App\Alumno::orderBy('no_exp', 'ASC')->paginate(25);
        }
        return view('/alumnos/index',compact('alumnos'));
    }

    public function show($id)
    {
        define ('EN_PROCESO', 1);
        define ('TERMINADO', 2);
        define ('CANCELADO', 2);
        $alumno = \App\Alumno::find($id);
        //APLICO FILTRO PARA COMPROBAR QUE SERVICIO SOCIAL ESTA DISPONIBLE
        $servicioSocial = \App\MateriasAlumno::
        //SERVICIO ACTUAL
        where('no_exp', $id)
        ->where('cve_asig','SSO01C1')->

        //SERVICIO ANTERIOR
        OrWhere('no_exp', $id)->
        where('cve_asig', 'SSO0112C14')
        ->first();

        //OBTIENE LOS SERVICIOS SOCIALES
        $registrosServicio[]=array();
        $registrosServicio = \App\Alumno::find($id)->servicios->sortByDesc('id_servicio');
    
        $proyecto[]=array();

        
     
        //ASESOR

        switch ($this->unidad) {
            case 1: $nombre="rechum_sl";break;
            case 2: $nombre="rechum_hillo";break;
            case 3: $nombre="rechum_nav";break;
            case 4: $nombre="rechum_mag";break;
            case 5: $nombre="rechum_bj";break;
            case 7: $nombre="rechum_isspe";break;
            default:$nombre="rechum_sl";break;
        }


        $asesor = new \App\Servicio;
        $asesor->setConnection($nombre.'.rh_personal');
        foreach ($registrosServicio as $servicio) {
            $dato =$asesor::find($servicio->id_servicio)->datos_asesor;            
            $asesor[$servicio->id_servicio]=array($servicio->id_servicio,$dato->nombre,$dato->ap_pat,$dato->ap_mat );
        }

      
        
        //PROYECTO
        foreach ($registrosServicio as $servicio) {
            $dato =\App\Servicio::find($servicio->id_servicio)->datos_proyecto;            
            $proyecto[$servicio->id_servicio]=array($servicio->id_servicio,$dato->nombre);
        }

        
        
        //CONOCER SI ALUMNO TIENE UN SERVICIO EN PROCESO
        $estado=0;
        foreach ($registrosServicio as $registro) {

            if($registro->estado==EN_PROCESO){
                $estado=EN_PROCESO;
            }else if ($registro->estado==TERMINADO) {
                $estado=TERMINADO;
            }
        }

        if(isset($registrosServicio)){
            return view('alumnos/show',compact('alumno','id','proyecto','servicioSocial','estado','registrosServicio','asesor'));
        }else{
            return view('alumnos/show',compact('alumno','id'));
        }

    }

  
}
