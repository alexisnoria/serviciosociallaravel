<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AsesorController extends Controller
{
    protected $unidad;
    public function __construct(Request $request)
    {
        
        $this->middleware(function ($request, $next) {
            $this->unidad = Auth::user()->unidad;
            // Se elige el nombre de la base segun la unidad del usuario
            switch ($this->unidad) {
                case 1: $nombre="rechum_sl";break;
                case 2: $nombre="rechum_hillo";break;
                case 3: $nombre="rechum_nav";break;
                case 4: $nombre="rechum_mag";break;
                case 5: $nombre="rechum_bj";break;
                case 7: $nombre="rechum_isspe";break;
                default:$nombre="rechum_sl";break;
            }
            DB::disconnect('ueshermosillo');//Se desconecta de la base por defecto
            Config::set('database.connections.ueshermosillo.database', $nombre);//new database name, you want to connect to.
            return $next($request);
        });
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asesors=\App\Asesor::where('espadre', 1)->orderBy('num_emp', 'DESC')->paginate(20);
        return view('/asesors/index',compact('asesors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     /*
    public function create()
    {
        return view('/asesors/create');
    }
*/
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /*
     public function store(Request $request)
    {
        $asesor= new \App\Asesor;
        $asesor->nombre=strtoupper($request->get('nombre'));
       // $asesor->pe=strtoupper($request->get('pe'));
        $asesor->save();
        return redirect('asesors')->with('success', 'Asesor '.$asesor->nombre.' registrado correctamente!');
    }
*/
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $asesor = \App\Asesor::find($id);
        return view('asesors/show',compact('asesor','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /*
    public function edit($id)
    {
        $asesor = \App\Asesor::find($id);
        return view('asesors/edit',compact('asesor','id'));
    }
*/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     /*
    public function update(Request $request, $id)
    {
        $asesor= \App\Asesor::find($id);
        $asesor->nombre=strtoupper($request->get('nombre'));
        $asesor->pe=strtoupper($request->get('pe'));
        $asesor->save();
        return redirect('asesors')->with('success','Asesor actualizado correctamente!');
    }
*/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*
    public function destroy($id)
    {
        $asesor = \App\Asesor::find($id);
        $asesor->delete();
        return redirect('asesors')->with('success','Asesor eliminado correctamente!');
    }
    */
}
