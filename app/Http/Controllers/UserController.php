<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;


class UserController extends Controller
{
    public function profile()
    {
        $user = Auth::user();
        $unidad="";
       switch ($user->unidad) {
       case 1:$unidad="San Luis Rio Colorado";break;
       case 2:$unidad="Hermosillo";break;
       case 3:$unidad="Navojoa";break;
       case 4:$unidad="Magdalena";break;
       case 5:$unidad="Benito Ju&#225;rez";break;
       case 7:$unidad="Hermosillo - ISSPE";break;
       case 0:$unidad="No seleccionado";break;
    }
  

        return view('profile',compact('user','unidad',$user));
    }

    public function update_avatar(Request $request){

        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $user = Auth::user();

        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

        $request->avatar->storeAs('avatars',$avatarName);

        $user->avatar = $avatarName;
        $user->save();

        return back()
            ->with('success','Has subido exitosamente la imagen.');

    }
}
