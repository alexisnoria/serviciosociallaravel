<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyectos=\App\Proyecto::orderBy('id', 'DESC')->paginate(20);

       
        return view('/proyectos/index',compact('proyectos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          return view('/proyectos/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proyecto= new \App\Proyecto;
        $proyecto->nombre=$request->get('nombre');
        $proyecto->titular=$request->get('titular');
        $proyecto->direccion=$request->get('direccion');
        $proyecto->asesor=$request->get('asesor');
        $proyecto->receptora=$request->get('receptora');
        $proyecto->cargotitular=$request->get('cargotitular');
        $proyecto->tipo=$request->get('tipo');
        $proyecto->carreras=$request->get('carreras');
        $proyecto->integrantes=$request->get('integrantes');
        $proyecto->actividades=$request->get('actividades');
        $proyecto->telefono=$request->get('telefono');
        $proyecto->save();
        
        return redirect('proyectos')->with('success', 'Proyecto registrado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proyecto = \App\Proyecto::find($id);
        return view('proyectos/show',compact('proyecto','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = \App\Proyecto::find($id);
        return view('proyectos/edit',compact('proyecto','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyecto= \App\Proyecto::find($id);
        $proyecto->nombre=$request->get('nombre');
        $proyecto->titular=$request->get('titular');
        $proyecto->direccion=$request->get('direccion');
        $proyecto->asesor=$request->get('asesor');
        $proyecto->receptora=$request->get('receptora');
        $proyecto->cargotitular=$request->get('cargotitular');
        $proyecto->tipo=$request->get('tipo');
        $proyecto->carreras=$request->get('carreras');
        $proyecto->integrantes=$request->get('integrantes');
        $proyecto->actividades=$request->get('actividades');
        $proyecto->telefono=$request->get('telefono');

        $proyecto->save();
        
        return redirect('proyectos')->with('success','Proyecto actualizada correctamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyecto = \App\Proyecto::find($id);
        $proyecto->delete();
        return redirect('proyectos')->with('success','Proyecto eliminada correctamente!');
    }
}
