<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DateTime;

class ServicioController extends Controller
{
   
    public function create($id)
    {
        $alumno = \App\Alumno::find($id);
        //return view('alumnos/show',compact('alumno','id'));
        return view('/servicio/create',compact('alumno','id'));
    }
    public function edit($id)
    {
        $servicio =\App\Servicio::find($id);

        $alumno=\App\Alumno::find($servicio->expediente);

        $proyecto_dato =\App\Servicio::find($id)->datos_proyecto;            
        $proyecto_nombre=$proyecto_dato->nombre;

        $asesor_dato =\App\Servicio::find($id)->datos_asesor;            
        $asesor_nombre=$asesor_dato->nombre." ".$asesor_dato->ap_pat." ".$asesor_dato->ap_mat;


        return view('/servicio/edit',compact('servicio','proyecto_nombre','asesor_nombre','alumno','id'));
    }

    public function update(Request $request, $id)
    {
      
        $servicio=  \App\Servicio::find($id);
        $servicio->proyecto=$request->get('proyecto_id');
        $servicio->asesor=$request->get('asesor_id');
        $servicio->dia_inicio=$request->get('dia_inicio');
        $hora_inicio = date('H:i:s', strtotime($request->get('hora_inicio')));
        $hora_final = date('H:i:s', strtotime($request->get('hora_final')));
        $servicio->hora_inicio=$hora_inicio;
        $servicio->hora_final=$hora_final;
        $servicio->save();
        

     //   $alumno=\App\Alumno::find($servicio->expediente);
     //   $proyecto_dato =\App\Servicio::find($id)->datos_proyecto;            
     //   $proyecto_nombre=$proyecto_dato->nombre;
     //   $asesor_dato =\App\Servicio::find($id)->datos_asesor;            
      //  $asesor_nombre=$asesor_dato->nombre." ".$asesor_dato->ap_pat." ".$asesor_dato->ap_mat;
      //  return view('/servicio/show',compact('servicio','proyecto_nombre','asesor_nombre','alumno','id'))->with('success','Empresa actualizada correctamente!');
    //  return redirect('empresas')->with('success','Empresa actualizada correctamente!');

      return redirect('/servicio/detalle/'.$id)->with('success', 'Servicio Social actualizado correctamente!');
    }




    public function store(Request $request)
    {
        $expediente=$request->get('expediente');
        $servicio= new \App\Servicio;
        $servicio->expediente=$expediente;
        $servicio->proyecto=$request->get('proyecto_id');
        $servicio->asesor=$request->get('asesor_id');
        $servicio->dia_inicio=$request->get('dia_inicio');
        $hora_inicio = date('H:i:s', strtotime($request->get('hora_inicio')));
        $hora_final = date('H:i:s', strtotime($request->get('hora_final')));
        $servicio->hora_inicio=$hora_inicio;
        $servicio->hora_final=$hora_final;
        $servicio->save();

        $alumno = \App\Alumno::find($expediente);
        //APLICO FILTRO PARA COMPROBAR QUE SERVICIO SOCIAL ESTA DISPONIBLE
        $servicioSocial = \App\MateriasAlumno::
        //SERVICIO ACTUAL
        where('no_exp', $expediente)
        ->where('cve_asig','SSO01C1')->

        //SERVICIO ANTERIOR
        OrWhere('no_exp', $expediente)->
        where('cve_asig', 'SSO0112C14')
        ->first();

       // return rediredic('alumnos/show',compact('alumno','expediente','servicioSocial'))->with('success','Servicio Social');

        return redirect('alumnos/'.$expediente)->with('success','Servicio Social registrado correctamente!');

       // return redirect('alumnos')->with('success', 'Servicio Social registrada correctamente!');
    }




    public function show($id)
    {
        /*
        define ('EN_PROCESO', 1);
        $alumno = \App\Alumno::find($id);
        //APLICO FILTRO PARA COMPROBAR QUE SERVICIO SOCIAL ESTA DISPONIBLE
        $servicioSocial = \App\MateriasAlumno::
        //SERVICIO ACTUAL
        where('no_exp', $id)
        ->where('cve_asig','SSO01C1')->

        //SERVICIO ANTERIOR
        OrWhere('no_exp', $id)->
        where('cve_asig', 'SSO0112C14')
        ->first();

        //OBTIENE LOS SERVICIOS SOCIALES
        $registrosServicio[]=array();
        $registrosServicio = \App\Alumno::find($id)->servicios->sortByDesc('id_servicio');
    
        $proyecto[]=array();

        
     
        //ASESOR

        switch ($this->unidad) {
            case 1: $nombre="rechum_sl";break;
            case 2: $nombre="rechum_hillo";break;
            case 3: $nombre="rechum_nav";break;
            case 4: $nombre="rechum_mag";break;
            case 5: $nombre="rechum_bj";break;
            case 7: $nombre="rechum_isspe";break;
            default:$nombre="rechum_sl";break;
        }


        $asesor = new \App\Servicio;
        $asesor->setConnection($nombre.'.rh_personal');
        foreach ($registrosServicio as $servicio) {
            $dato =$asesor::find($servicio->id_servicio)->datos_asesor;            
            $asesor[$servicio->id_servicio]=array($servicio->id_servicio,$dato->nombre,$dato->ap_pat,$dato->ap_mat );
        }

      
        
        //PROYECTO
        foreach ($registrosServicio as $servicio) {
            $dato =\App\Servicio::find($servicio->id_servicio)->datos_proyecto;            
            $proyecto[$servicio->id_servicio]=array($servicio->id_servicio,$dato->nombre);
        }

        
        
        //CONOCER SI ALUMNO TIENE UN SERVICIO EN PROCESO
        $en_proceso=false;
        foreach ($registrosServicio as $registro) {
            if($registro->estado==EN_PROCESO){
                $en_proceso=true;
            }
        }

        //dd($registrosServicio);
        if(isset($registrosServicio)){
            return view('alumnos/show',compact('alumno','id','proyecto','servicioSocial','en_proceso','registrosServicio','asesor'));
        }else{
            return view('alumnos/show',compact('alumno','id'));
        }
        */

        $servicio =\App\Servicio::find($id);

        $alumno=\App\Alumno::find($servicio->expediente);

        $proyecto_dato =\App\Servicio::find($id)->datos_proyecto;            
        $proyecto_nombre=$proyecto_dato->nombre;


        $asesor_dato =\App\Servicio::find($id)->datos_asesor;            
        $asesor_nombre=$asesor_dato->nombre." ".$asesor_dato->ap_pat." ".$asesor_dato->ap_mat;


        return view('/servicio/show',compact('servicio','proyecto_nombre','asesor_nombre','alumno','id'));
    }
  
}
