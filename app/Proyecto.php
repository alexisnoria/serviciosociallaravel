<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    public function servicio()
    {
        return $this->belongsTo('App\Servicio','proyecto','id');
    }
}
