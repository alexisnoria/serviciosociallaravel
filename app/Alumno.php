<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Laravel\Scout\Searchable;
class Alumno extends Model
{
    use Searchable;
     /**
     * The database connection used by the model.
     *
     * @var string
     */
  
    protected $connection = 'ueshermosillo';
    
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'ce_alumnos';

    protected $primaryKey = 'no_exp';

    public $incrementing = false;
    protected $dateFormat = 'U';
    public function searchableAs()
    {
        return 'alumnos_index';
    }


    public function materias() {
        return $this->hasMany('ce_alum_calif', 'no_exp');
    }

    public function servicios()
    {
        return $this->hasMany('App\Servicio','expediente', 'no_exp');
    }



}
